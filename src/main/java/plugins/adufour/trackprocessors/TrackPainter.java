package plugins.adufour.trackprocessors;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import icy.canvas.IcyCanvas;
import icy.canvas.IcyCanvas2D;
import icy.painter.Overlay;
import icy.painter.OverlayWrapper;
import icy.painter.VtkPainter;
import icy.sequence.Sequence;
import icy.type.point.Point5D;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarBoolean;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackManagerPainter;
import plugins.fab.trackmanager.TrackPool;
import plugins.fab.trackmanager.TrackSegment;
import plugins.kernel.canvas.VtkCanvas;
import plugins.nchenouard.spot.Detection;
import vtk.vtkActor;
import vtk.vtkAppendPolyData;
import vtk.vtkGlyph3D;
import vtk.vtkLODActor;
import vtk.vtkParametricFunctionSource;
import vtk.vtkParametricSpline;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkProp;
import vtk.vtkRenderer;
import vtk.vtkSphereSource;
import vtk.vtkTubeFilter;
import vtk.vtkUnsignedCharArray;

public class TrackPainter extends PluginTrackManagerProcessor
{
    private static final String filterName = "Display tracks on sequence";

    private TrackOverlay overlay;
    private boolean keepLegacyOverlay = true;
    private Overlay legacyOverlay;

    private List<TrackSegment> tracks;

    public TrackPainter()
    {
        super();

        setName(filterName);
        tracks = new ArrayList<TrackSegment>();
    }

    @Override
    public void Close()
    {
        if (overlay != null)
            overlay.remove();

        // put the legacy overlay back in place
        if (keepLegacyOverlay && legacyOverlay != null)
        {
            if (trackPool.getDisplaySequence() != null)
                trackPool.getDisplaySequence().addOverlay(legacyOverlay);

            legacyOverlay = null;
        }
    }

    @Override
    public void displaySequenceChanged()
    {
        System.out.println("display sequence changed");
        if (overlay != null)
        {
            overlay.remove();
            overlay = null;
        }
        Compute();
    }

    @Override
    public void Compute()
    {
        if (overlay == null)
        {
            Sequence s = trackPool.getDisplaySequence();
            if (s != null)
            {
                // remove the current legacy painter and store it here
                for (Overlay layer : s.getOverlays(OverlayWrapper.class))
                {
                    OverlayWrapper wrapper = (OverlayWrapper) layer;
                    if (wrapper.getPainter() instanceof TrackManagerPainter)
                    {
                        legacyOverlay = layer;
                        s.removeOverlay(legacyOverlay);
                    }
                }

                s.addOverlay(overlay = new TrackOverlay(trackPool));
                panel.setVisible(false);
                panel.setLayout(new BorderLayout());
                panel.add(overlay.getOptionsPanel(), BorderLayout.CENTER);
                panel.setVisible(true);
                panel.invalidate();
            }
        }
        else
        {
            // overlay.upToDate = false;
        }

        // store current state of tracks (so we display it correctly depending the order of the processor)
        final List<TrackSegment> newTracks = new ArrayList<TrackSegment>();
        for (TrackSegment track : trackPool.getTrackSegmentList())
        {
            final ArrayList<Detection> detections = new ArrayList<Detection>();

            try
            {
                for (Detection detection : track.getDetectionList())
                    detections.add((Detection) detection.clone());
            }
            catch (CloneNotSupportedException e)
            {
                // should never happen
                e.printStackTrace();
            }

            newTracks.add(new TrackSegment(detections));
        }
        // better for co-modification
        tracks = newTracks;

        // need to refresh display
        if (overlay != null)
            overlay.upToDate = false;
    }

    public class TrackOverlay extends Overlay implements VtkPainter
    {
        private final TrackPool trackPool;

        private double xScale, yScale, zScale;
        private int positionT;

        private final vtkSphereSource sphere = new vtkSphereSource();
        private final vtkPolyData spherePolyData = new vtkPolyData();
        private final vtkGlyph3D spheres = new vtkGlyph3D();
        private final vtkPolyDataMapper sphereMapper = new vtkPolyDataMapper();
        private final vtkActor sphereActor = new vtkLODActor();

        private final vtkAppendPolyData lines = new vtkAppendPolyData();
        private final vtkPolyDataMapper lineMapper = new vtkPolyDataMapper();
        private final vtkActor lineActor = new vtkActor();

        private final List<vtkActor> visibleActors = new ArrayList<vtkActor>(2);

        private IcyCanvas canvas;
        private vtkRenderer renderer = null;

        private boolean upToDate = false;

        // user settings (detections)
        private final VarBoolean detectionDisplay = new VarBoolean("Show detections", true)
        {
            protected void fireVariableChanged(Boolean oldValue, Boolean newValue)
            {
                setDetectionDisplay(newValue);
                super.fireVariableChanged(oldValue, newValue);
            }
        };

        // user settings (tracks)
        private final VarBoolean trackDisplay = new VarBoolean("Show tracks", true)
        {
            protected void fireVariableChanged(Boolean oldValue, Boolean newValue)
            {
                setTrackDisplay(newValue);
                super.fireVariableChanged(oldValue, newValue);
            }
        };

        private final VarBoolean displaySelectionOnly = new VarBoolean("Show selected tracks", false)
        {
            protected void fireVariableChanged(Boolean oldValue, Boolean newValue)
            {
                super.fireVariableChanged(oldValue, newValue);
                upToDate = false;
                painterChanged();
            }
        };

        // 3D settings

        private final VarBoolean autoScale3D = new VarBoolean("Auto-scale with zoom", true)
        {
            protected void fireVariableChanged(Boolean oldValue, Boolean newValue)
            {
                if (newValue)
                {
                    upToDate = false;
                    painterChanged();
                }
                super.fireVariableChanged(oldValue, newValue);
            }
        };

        private final VarBoolean trackTubes = new VarBoolean("Show tracks as tubes", true)
        {
            protected void fireVariableChanged(Boolean oldValue, Boolean newValue)
            {
                setTrackDisplayAsTubes(newValue);
                super.fireVariableChanged(oldValue, newValue);
            }
        };

        public TrackOverlay(TrackPool trackPool)
        {
            super(filterName);

            this.trackPool = trackPool;

            final Sequence seq = trackPool.getDisplaySequence();

            if (seq == null)
            {
                xScale = 1d;
                yScale = 1d;
                zScale = 1d;
            }
            else
            {
                xScale = seq.getPixelSizeX();
                yScale = seq.getPixelSizeY();
                zScale = seq.getPixelSizeZ();
            }
            positionT = 0;

            // the overlay cannot be removed alone, it must be removed via the Track Manager
            setCanBeRemoved(false);

            // Create the user interface
            // createGUI(getOptionsPanel());
        }

        public JPanel getOptionsPanel()
        {
            JPanel container = new JPanel();
            container.setBorder(null);
            container.setLayout(new BoxLayout(container, BoxLayout.X_AXIS));

            // Create a panel for the detections

            JPanel detectionPanel = new JPanel(new GridBagLayout());
            detectionPanel.setBorder(new TitledBorder("Common settings"));
            container.add(detectionPanel);

            for (Var<?> var : new Var<?>[] {detectionDisplay, trackDisplay, displaySelectionOnly})
            {
                GridBagConstraints gbc = new GridBagConstraints();

                gbc.insets = new Insets(2, 10, 2, 5);
                gbc.fill = GridBagConstraints.HORIZONTAL;
                detectionPanel.add(new JLabel(var.getName()), gbc);

                gbc.weightx = 1;
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                SwingVarEditor<?> editor = (SwingVarEditor<?>) var.createVarEditor();
                editor.setEnabled(true);
                editor.getEditorComponent().setFocusable(false);
                detectionPanel.add(editor.getEditorComponent(), gbc);
            }

            // Create a panel for the tracks

            JPanel trackPanel = new JPanel(new GridBagLayout());
            trackPanel.setBorder(new TitledBorder("3D settings"));
            container.add(trackPanel);

            for (Var<?> var : new Var<?>[] {autoScale3D, trackTubes})
            {
                GridBagConstraints gbc = new GridBagConstraints();

                gbc.insets = new Insets(2, 10, 2, 5);
                gbc.fill = GridBagConstraints.HORIZONTAL;
                trackPanel.add(new JLabel(var.getName()), gbc);

                gbc.weightx = 1;
                gbc.gridwidth = GridBagConstraints.REMAINDER;
                SwingVarEditor<?> editor = (SwingVarEditor<?>) var.createVarEditor();
                editor.setEnabled(true);
                editor.getEditorComponent().setFocusable(false);
                trackPanel.add(editor.getEditorComponent(), gbc);
            }

            return container;
        }

        /**
         * Initializes (immutable) rendering components and default user settings
         * 
         * @param vtkCanvas
         */
        private void initRenderer(VtkCanvas vtkCanvas)
        {
            // 1) initialize immutable items

            renderer = vtkCanvas.getRenderer();
            canvas = vtkCanvas;

            // detections
            sphere.SetThetaResolution(16);
            sphere.SetPhiResolution(16);
            spheres.ScalingOff();
            spheres.SetColorModeToColorByScalar();
            spheres.SetInputData(spherePolyData);
            spheres.SetSourceConnection(sphere.GetOutputPort());
            sphereMapper.SetInputConnection(spheres.GetOutputPort());
            sphereActor.SetMapper(sphereMapper);
            // add it (but invisible for now)
            sphereActor.SetVisibility(0);

            // tracks
            lineMapper.SetInputConnection(lines.GetOutputPort());
            lineActor.SetMapper(lineMapper);
            // add it (but invisible for now)
            lineActor.SetVisibility(0);

            // 2) activate display

            visibleActors.clear();
            setDetectionDisplay(detectionDisplay.getValue());
            setTrackDisplay(trackDisplay.getValue());
        }

        /**
         * Enables/Disables detection display. See below for further display options
         * 
         * @param display
         *        <code>true</code> if detections should be displayed, <code>false</code>
         *        otherwise
         * see #setDetectionRadius(double)
         */
        public void setDetectionDisplay(boolean display)
        {
            this.detectionDisplay.setValue(display);

            // VTK uses integers...
            final int visibility = display ? 1 : 0;

            if ((renderer != null) && (sphereActor.GetVisibility() != visibility))
                sphereActor.SetVisibility(visibility);

            painterChanged();
        }

        /**
         * Enables/Disables track display. See below for further display options
         * 
         * @param display
         *        <code>true</code> if tracks should be displayed, <code>false</code> otherwise
         * @see #setTrackDisplayAsTubes(boolean)
         * see #setTrackTubeRadius(double)
         */
        public void setTrackDisplay(boolean display)
        {
            this.trackDisplay.setValue(display);

            // VTK uses integers...
            final int visibility = display ? 1 : 0;

            if ((renderer != null) && (lineActor.GetVisibility() != visibility))
                lineActor.SetVisibility(visibility);

            painterChanged();
        }

        /**
         * @param displayAsTubes
         *        <code>true</code> if the tracks should be displayed as 3D tubes, or
         *        <code>false</code> to display tracks as lines (no thickness). Note that
         *        displaying tracks as 3D tubes can be considerably slower on some systems
         * see #setTrackTubeRadius(double)
         */
        public void setTrackDisplayAsTubes(boolean displayAsTubes)
        {
            this.trackTubes.setValue(displayAsTubes);

            upToDate = false;
            painterChanged();
        }

        @Override
        public void paint(Graphics2D g, Sequence sequence, IcyCanvas theCanvas)
        {
            if (theCanvas instanceof VtkCanvas)
            {
                VtkCanvas vtk = (VtkCanvas) theCanvas;

                if (renderer != vtk.getRenderer())
                {
                    initRenderer(vtk);
                    upToDate = false;
                }

                if ((sequence.getPixelSizeX() != xScale) || (sequence.getPixelSizeY() != yScale)
                        || (sequence.getPixelSizeZ() != zScale))
                {
                    xScale = sequence.getPixelSizeX();
                    yScale = sequence.getPixelSizeY();
                    zScale = sequence.getPixelSizeZ();
                    upToDate = false;
                }

                if (theCanvas.getPositionT() != positionT)
                {
                    positionT = theCanvas.getPositionT();
                    // need to check that because of the screenshot rendering which doesn't send position change event
                    if (trackPool.getTrackManager().getCurrentPositionT() != positionT)
                        trackPool.getTrackManager().setCurrentPositionT(positionT);

                    upToDate = false;
                }

                if (upToDate)
                    return;

                if (detectionDisplay.getValue())
                    drawDetections3D(theCanvas.getPositionT());
                if (trackDisplay.getValue())
                    drawTracks3D();

                upToDate = true;
            }
            else if (theCanvas instanceof IcyCanvas2D)
            {
                drawTracks2D((Graphics2D) g.create(), (IcyCanvas2D) theCanvas);
            }
        }

        private void drawTracks2D(Graphics2D g, IcyCanvas2D theCanvas)
        {
            // need to check that because of the screenshot rendering which doesn't send position change event
            if (trackPool.getTrackManager().getCurrentPositionT() != theCanvas.getPositionT())
                trackPool.getTrackManager().setCurrentPositionT(theCanvas.getPositionT());

            Graphics2D g2 = (Graphics2D) g.create();
            double zoom = theCanvas.getScaleX();

            double lodThreshold = 2.0 / zoom;

            float strokeWidth = (float) theCanvas.canvasToImageLogDeltaX(2);

            g.setStroke(new BasicStroke(strokeWidth));

            Point2d p1 = new Point2d(), p2 = new Point2d();
            Line2D.Double l = new Line2D.Double();

            double shapeFactor = 5.0;
            Ellipse2D.Double detectionShape = new Ellipse2D.Double(0, 0, strokeWidth * shapeFactor,
                    strokeWidth * shapeFactor);
            double shapeCenter = strokeWidth * shapeFactor / 2;

            g.translate(0.5, 0.5);

            for (TrackSegment ts : tracks)
            {
                ArrayList<Detection> detections = ts.getDetectionList();

                int nDet = detections.size();

                for (int t = 0; t < nDet; t++)
                {
                    Detection d1 = detections.get(t);
                    if (d1 == null || !d1.isEnabled())
                        continue;
                    p1.set(d1.getX(), d1.getY());
                    g.setColor(d1.getColor());

                    if (displaySelectionOnly.getValue())
                    {
                        if (!d1.isSelected())
                            continue;
                    }
                    else if (d1.isSelected())
                        g.setColor(Color.white);

                    if (d1.getT() == theCanvas.getPositionT() && d1 != null && detectionDisplay.getValue())
                    {
                        // draw current detection
                        // if selected, switch color to white
                        detectionShape.x = p1.x - shapeCenter;
                        detectionShape.y = p1.y - shapeCenter;
                        g.draw(detectionShape);
                        d1.paint(g2, theCanvas.getSequence(), theCanvas);
                    }

                    // LOD find the shortest segment above the drawing threshold
                    for (; t < nDet - 1; t++)
                    {
                        Detection d2 = detections.get(t + 1);
                        if (d2 == null || !d2.isEnabled())
                            break;

                        if (trackDisplay.getValue())
                        {
                            p2.set(d2.getX(), d2.getY());

                            if (p1.distanceSquared(p2) < lodThreshold)
                            {
                                // draw the detection in case it was skipped due to the LOD
                                if (d2.getT() == theCanvas.getPositionT() && d2 != null && detectionDisplay.getValue())
                                {
                                    // draw current detection
                                    // if selected, switch color to white
                                    detectionShape.x = p2.x - shapeCenter;
                                    detectionShape.y = p2.y - shapeCenter;
                                    g.draw(detectionShape);
                                    d2.paint(g2, theCanvas.getSequence(), theCanvas);
                                }
                                // now skip
                                continue;
                            }

                            l.setLine(p1.x, p1.y, p2.x, p2.y);
                            g.draw(l);
                            // t--;
                        }

                        break;
                    }
                }

            }
        }

        private void drawTracks3D()
        {
            double viewScale = Math.sqrt(((VtkCanvas) canvas).getCamera().GetDistance()) * (xScale + yScale + zScale)
                    / 3;
            double tubeRadius = viewScale / 15.0;

            int tubeSection = (int) Math.min(8, 100.0 / viewScale);
            int tubeSmoothness = Math.max(1, tubeSection - 1);

            lines.RemoveAllInputs();

            for (TrackSegment ts : tracks)
            {
                List<Detection> detections = ts.getDetectionList();

                vtkPoints points = new vtkPoints();
                vtkUnsignedCharArray colors = new vtkUnsignedCharArray();
                colors.SetName("colors");
                colors.SetNumberOfComponents(3);

                for (int t = 0; t < detections.size(); t++)
                {
                    Detection detection = detections.get(t);

                    if (detection == null)
                        continue;
                    if (displaySelectionOnly.getValue() && !detection.isSelected())
                        continue;

                    if (detection.isEnabled())
                    {
                        Point3d position = new Point3d(detection.getX() * xScale, detection.getY() * yScale,
                                detection.getZ() * zScale);
                        Color color = (displaySelectionOnly.getValue() == detection.isSelected()) ? detection.getColor()
                                : Color.white;

                        points.InsertNextPoint(position.x, position.y, position.z);

                        for (int i = 0; i < tubeSmoothness; i++)
                            colors.InsertNextTuple3(color.getRed(), color.getGreen(), color.getBlue());
                    }
                }

                vtkParametricSpline spline = new vtkParametricSpline();
                spline.SetPoints(points);

                vtkParametricFunctionSource function = new vtkParametricFunctionSource();
                function.SetParametricFunction(spline);
                function.SetUResolution(points.GetNumberOfPoints() * tubeSmoothness);
                function.Update();
                vtkPolyData lineData = function.GetOutput();
                lineData.GetPointData().SetScalars(colors);

                if (trackTubes.getValue())
                {
                    // draw the splines as thick 3D tubes
                    vtkTubeFilter tuber = new vtkTubeFilter();
                    tuber.SetInputData(lineData);
                    tuber.SetRadius(tubeRadius);
                    tuber.CappingOn();
                    // tuber.SetNumberOfSides(trackTubeSides.getValue());

                    tuber.SetNumberOfSides((int) Math.min(8, 100.0 / viewScale));
                    tuber.SidesShareVerticesOn();

                    tuber.Update();
                    lines.AddInputData(tuber.GetOutput());

                    // cleanup
                    tuber.Delete();
                }
                else
                {
                    // just draw the spline itself (no thickness)
                    lines.AddInputData(lineData);
                }

                lines.Update();
                lineMapper.Update();

                // cleanup
                lineData.Delete();
                function.Delete();
                spline.Delete();
                colors.Delete();
                points.Delete();
            }
        }

        private void drawDetections3D(int positionT)
        {
            double viewScale = (xScale + yScale + zScale) / 3;
            double sphereRadius = Math.sqrt(((VtkCanvas) canvas).getCamera().GetDistance()) * viewScale / 5.0;
            sphere.SetRadius(sphereRadius);

            vtkPoints spherePositions = new vtkPoints();
            vtkUnsignedCharArray sphereColors = new vtkUnsignedCharArray();
            sphereColors.SetName("colors");
            sphereColors.SetNumberOfComponents(3);

            for (Detection detection : trackPool.getAllDetection())
            {
                if (detection == null || !detection.isEnabled())
                    continue;

                if (positionT >= 0 && positionT != detection.getT())
                    continue;

                if (displaySelectionOnly.getValue() && !detection.isSelected())
                    continue;

                Point3d position = new Point3d(detection.getX() * xScale, detection.getY() * yScale,
                        detection.getZ() * zScale);
                Color color = (displaySelectionOnly.getValue() == detection.isSelected()) ? detection.getColor()
                        : Color.white;

                spherePositions.InsertNextPoint(position.x, position.y, position.z);
                sphereColors.InsertNextTuple3(color.getRed(), color.getGreen(), color.getBlue());
            }

            spherePolyData.SetPoints(spherePositions);
            spherePolyData.GetPointData().SetScalars(sphereColors);
            spherePolyData.GetPointData().Update();
            sphereMapper.Update();

            sphereColors.Delete();
            spherePositions.Delete();
        }

        @Override
        public void remove()
        {
            if (renderer != null)
                renderer = null;

            super.remove();
        }

        @Override
        public void mouseClick(MouseEvent e, Point5D.Double imagePoint, IcyCanvas theCanvas)
        {
            super.mouseClick(e, imagePoint, theCanvas);

            if (!(theCanvas instanceof IcyCanvas2D))
                return;

            imagePoint.x -= 0.5;
            imagePoint.y -= 0.5;

            double selectThreshold = Math.min(10.0, Math.max(0.25, 10.0 / theCanvas.getScaleX()));

            // List all candidate tracks close to the click
            // => store this in a map of [distance <=> track]
            TreeMap<Double, TrackSegment> candidates = new TreeMap<Double, TrackSegment>();

            for (TrackSegment ts : trackPool.getTrackSegmentList())
            {
                ArrayList<Detection> detections = ts.getDetectionList();

                int nDet = detections.size() - 1;

                detectionLoop: for (int t = 0; t < nDet; t++)
                {
                    // Retrieve the "d1-d2" segment
                    Detection d1 = detections.get(t);
                    Detection d2 = detections.get(t + 1);

                    if (d1 == null || d2 == null || !d1.isEnabled() || !d2.isEnabled())
                        continue;

                    // Check if the click is close to the segment
                    double distSq = Line2D.ptSegDistSq(d1.getX(), d1.getY(), d2.getX(), d2.getY(), imagePoint.x,
                            imagePoint.y);

                    if (distSq < selectThreshold)
                    {
                        candidates.put(distSq, ts);
                        // move to the next track segment
                        break detectionLoop;
                    }
                }
            }

            if (candidates.size() > 0)
            {
                // highlight the closest candidate
                for (Detection d : candidates.firstEntry().getValue().getDetectionList())
                    d.setSelected(!d.isSelected());

                trackPool.fireTrackEditorProcessorChange();
            }
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e, icy.type.point.Point5D.Double imagePoint, IcyCanvas theCanvas)
        {
            if (autoScale3D.getValue())
                upToDate = false;

            super.mouseWheelMoved(e, imagePoint, theCanvas);
        }

        @Override
        public vtkProp[] getProps()
        {
            return new vtkProp[] {lineActor, sphereActor};
        }
    }
}
